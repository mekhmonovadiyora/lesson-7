
const appendSeconds = document.getElementById('seconds')
const appendTens = document.getElementById('tens')
const start = document.getElementById('start')
const pause = document.getElementById('pause')
const reset = document.getElementById('reset')
let tens = 00
let seconds = 00

function startTimer () {
    tens++

    if (tens<9) appendTens.innerHTML = '0' + tens
    if (tens>9) appendTens.innerHTML = tens
    if (seconds>9) appendSeconds.innerHTML = seconds
    if (tens>99) {
        seconds++
        appendSeconds.innerHTML = '0' + seconds
        tens = 0
        appendTens.innerHTML = '0' + 0
    }
}

start.onclick = function () {
    interval = setInterval(startTimer, 10)
}

pause.onclick = function () {
    clearInterval(interval)
}

reset.onclick = function () {
    clearInterval(interval)
    tens = '00'
    seconds = '00'
    appendSeconds.innerHTML = seconds
    appendTens.innerHTML = tens
}